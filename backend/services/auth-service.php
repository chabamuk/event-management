<?php
require '../domain/user.php';

/**
 * Class AuthService
 * 
 * This class provides authentication services including user login and registration.
 */
class AuthService
{
    /**
     * @var PDO $pdo The PDO instance for database connection.
     */
    private $pdo;

    /**
     * AuthService constructor.
     * 
     * @param PDO $pdo The PDO instance for database connection.
     */
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Logs in a user by verifying their username and password.
     * 
     * @param string $username The username of the user.
     * @param string $password The password of the user.
     * 
     * @return void
     */
    public function login(string $username, string $password): void
    {
        $user = User::findByUsername($this->pdo, $username);
        if (!$user || !password_verify($password, $user->getPassword())) {
            sendResponse(false, "Invalid username or password.", 400);
        }
        session_start();
        $_SESSION['user_id'] = $user->getId();
        $_SESSION['role'] = $user->getRole();
        sendResponse();
    }

    /**
     * Registers a new user with the provided username, password, and email.
     * 
     * @param string $username The username of the new user.
     * @param string $password The password of the new user.
     * @param string $email The email of the new user.
     * 
     * @return void
     */
    public function register(string $username, string $password, string $email): void
    {
        $username = $this->validate_input($username);
        $password = $password ?? '';
        $email = $this->validate_input($email);

        $errors = $this->validate_registration($username, $email, $password);

        if (empty($errors)) {
            $errors = $this->check_existing_user($username, $email);
        }

        if (!empty($errors)) {
            sendResponse(false, implode("\n", $errors), 400);
        }

        $hashed_password = password_hash($password, PASSWORD_DEFAULT);

        if ($this->register_user($username, $hashed_password, $email)) {
            sendResponse(true);
        } else {
            sendResponse(false, "User registration failed.", 500);
        }
    }

    /**
     * Validates and sanitizes input data.
     * 
     * @param string $data The input data to be validated and sanitized.
     * 
     * @return string The sanitized input data.
     */
    private function validate_input(string $data): string
    {
        return trim($data ?? '');
    }

    /**
     * Validates the registration data including username, email, and password.
     * 
     * @param string $username The username to be validated.
     * @param string $email The email to be validated.
     * @param string $password The password to be validated.
     * 
     * @return array An array of error messages, if any.
     */
    private function validate_registration(string $username, string $email, string $password): array
    {
        $errors = [];

        if (empty($username)) {
            $errors[] = 'Username is required.';
        }

        if (empty($email)) {
            $errors[] = 'Email is required.';
        }

        if (empty($password)) {
            $errors[] = 'Password is required.';
        }

        if (strlen($password) < 8) {
            $errors[] = "Password must be at least 8 characters long.";
        }

        if (!preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/', $password)) {
            $errors[] = "Password must contain at least one uppercase letter, one lowercase letter, and one number.";
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors[] = 'Invalid email format.';
        }

        return $errors;
    }

    /**
     * Checks if a user with the given username or email already exists.
     * 
     * @param string $username The username to be checked.
     * @param string $email The email to be checked.
     * 
     * @return array An array of error messages, if any.
     */
    private function check_existing_user($username, $email): array
    {
        $errors = [];

        if ($this->user_exists('username', $username)) {
            $errors[] = 'Username already exists.';
        }

        if ($this->user_exists('email', $email)) {
            $errors[] = 'Email already registered.';
        }

        return $errors;
    }

    /**
     * Checks if a user exists in the database based on a specific field and value.
     * 
     * @param string $field The field to be checked (e.g., 'username' or 'email').
     * @param string $value The value to be checked.
     * 
     * @return bool True if the user exists, false otherwise.
     */
    private function user_exists(string $field, string $value): bool
    {
        $stmt = $this->pdo->prepare("SELECT COUNT(*) FROM User WHERE $field = ?");
        $stmt->execute([$value]);
        return $stmt->fetchColumn() > 0;
    }

    /**
     * Registers a new user in the database.
     * 
     * @param string $username The username of the new user.
     * @param string $hashed_password The hashed password of the new user.
     * @param string $email The email of the new user.
     * 
     * @return bool True if the user was successfully registered, false otherwise.
     */
    private function register_user(string $username, string $hashed_password, string $email): bool
    {
        $stmt = $this->pdo->prepare("INSERT INTO User (username, password, email) VALUES (?, ?, ?)");
        return $stmt->execute([$username, $hashed_password, $email]);
    }
}
