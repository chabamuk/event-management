<?php
require '../common/common.php';
require '../domain/event.php';
require '../contracts/paged-data.php';

/**
 * Class EventService
 * 
 * Provides services related to event management including CRUD operations, 
 * event attendance, and pagination of events.
 */
class EventService
{
    /**
     * @var PDO $pdo The PDO instance for database connection.
     */
    private $pdo;

    /**
     * EventService constructor.
     * 
     * @param PDO $pdo The PDO instance for database connection.
     */
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Retrieves paginated events with optional filters for user attendance.
     * 
     * @param int $page The page number to retrieve.
     * @param bool|null $isAttending Filter for events the user is attending or not.
     * @param int|null $userId The ID of the user to filter events for.
     * @return PagedEventsResponse The paginated events response.
     * @throws Exception If a database error occurs.
     */
    public function getPagedEvents(int $page, ?bool $isAttending, ?int $userId): PagedEventsResponse
    {
        $limit = 20;
        $offset = ($page - 1) * $limit;

        try {
            if (!$this->pdo) {
                throw new PDOException("Database connection failed");
            }

            if ($userId && $isAttending !== null) {
                if ($isAttending) {
                    $stmt = $this->pdo->prepare("
                    SELECT e.id, e.title, e.description, e.date, e.location 
                    FROM `Event` e
                    JOIN `Registration` r ON e.id = r.event_id
                    WHERE r.user_id = :userId
                    LIMIT :limit OFFSET :offset
                ");
                } else {
                    $stmt = $this->pdo->prepare("
                    SELECT e.id, e.title, e.description, e.date, e.location 
                    FROM `Event` e
                    LEFT JOIN `Registration` r ON e.id = r.event_id AND r.user_id = :userId
                    WHERE r.event_id IS NULL
                    LIMIT :limit OFFSET :offset
                ");
                }
                $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
            } else {
                $stmt = $this->pdo->prepare("SELECT id, title, description, date, location FROM `Event` LIMIT :limit OFFSET :offset");
            }

            $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
            $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
            $stmt->execute();
            $events = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if ($events === false) {
                throw new PDOException("Failed to fetch events");
            }

            $totalEvents = $this->pdo->query("SELECT COUNT(*) as total FROM `Event`")->fetchColumn();
            if ($totalEvents === false) {
                throw new PDOException("Failed to fetch total events count");
            }

            $totalPages = ceil($totalEvents / $limit);

            $response = new PagedEventsResponse($events, $totalPages, $page, null, null);

            if ($userId) {
                $stmt = $this->pdo->prepare('
                SELECT event_id
                FROM Registration
                WHERE user_id = ?');
                $stmt->execute([$userId]);
                $attendedEvents = $stmt->fetchAll(PDO::FETCH_COLUMN);
                $response->attendedEvents = $attendedEvents;

                if ($_SESSION['role'] === 'Admin') {
                    $response->canModifyEvents = array_column($events, 'id');
                } else {
                    $stmt = $this->pdo->prepare('
                    SELECT id
                    FROM Event
                    WHERE created_by = :userId
                    LIMIT :limit OFFSET :offset');
                    $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
                    $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
                    $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
                    $stmt->execute();
                    $response->canModifyEvents = $stmt->fetchAll(PDO::FETCH_COLUMN);
                }
            }

            return $response;
        } catch (PDOException $e) {
            throw new Exception('Database error: ' . $e->getMessage());
        }
    }

    /**
     * Creates a new event.
     * 
     * @param string $title The title of the event.
     * @param string $datetime The date and time of the event.
     * @param string $location The location of the event.
     * @param string $description The description of the event.
     * @param int $user_id The ID of the user creating the event.
     */
    public function createEvent(string $title, string $datetime, string $location, string $description, int $user_id)
    {
        $validationError = $this->validateEventInput($title, $datetime, $location, $description);
        if ($validationError) {
            sendResponse(false, $validationError, 400);
        }

        $stmt = $this->pdo->prepare("INSERT INTO `Event` (title, date, location, description, created_by, created_at) VALUES (?, ?, ?, ?, ?, ?)");
        $createdAt = date('Y-m-d H:i:s');

        if ($stmt->execute([$title, $datetime, $location, $description, $user_id, $createdAt])) {
            sendResponse(true);
        } else {
            sendResponse(false, 'Failed to create the event.', 500);
        }
    }

    /**
     * Edits an existing event.
     * 
     * @param int $eventId The ID of the event to edit.
     * @param string $title The new title of the event.
     * @param string $datetime The new date and time of the event.
     * @param string $location The new location of the event.
     * @param string $description The new description of the event.
     * @param int $user_id The ID of the user editing the event.
     */
    public function editEvent(int $eventId, string $title, string $datetime, string $location, string $description, int $user_id)
    {
        $validationError = $this->validateEventInput($title, $datetime, $location, $description);
        if ($validationError) {
            sendResponse(false, $validationError, 400);
        }

        $stmt = $this->pdo->prepare("UPDATE `Event` SET title = ?, date = ?, location = ?, description = ?, updated_by = ?, updated_at = ? WHERE id = ?");
        $updatedAt = date('Y-m-d H:i:s');

        if ($stmt->execute([$title, $datetime, $location, $description, $user_id, $updatedAt, $eventId])) {
            sendResponse(true);
        } else {
            sendResponse(false, 'Failed to update the event.', 500);
        }
    }

    /**
     * Deletes an event.
     * 
     * @param int $eventId The ID of the event to delete.
     * @param int $userId The ID of the user attempting to delete the event.
     */
    public function deleteEvent(int $eventId, int $userId)
    {
        $eventOwnerId = $this->getEventOwnerId($eventId);

        if (!$eventOwnerId) {
            sendResponse(false, 'Event not found', 400);
        }

        if (!$this->canUserDeleteEvent($eventOwnerId, $userId)) {
            sendResponse(false, 'You are not allowed to delete this event', 403);
        }

        $stmt = $this->pdo->prepare("DELETE FROM `Event` WHERE id = ?");
        if ($stmt->execute([$eventId])) {
            sendResponse(true);
        } else {
            sendResponse(false, 'Failed to delete the event', 500);
        }
    }

    /**
     * Retrieves an event by its ID.
     * 
     * @param int $eventId The ID of the event to retrieve.
     * @return Event|null The event object or null if not found.
     */
    public function getEventById(int $eventId): Event|null
    {
        $stmt = $this->pdo->prepare("SELECT id, title, description, date, location, created_by FROM `Event` WHERE id = :eventId");
        $stmt->bindParam(':eventId', $eventId, PDO::PARAM_INT);
        $stmt->execute();
        $event = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($event === false) {
            return null;
        }

        return new Event($event['id'], $event['title'], $event['description'], $event['date'], $event['location'], $event['created_by']);
    }

    /**
     * Registers a user to attend an event.
     * 
     * @param int $event_id The ID of the event to attend.
     * @param int $user_id The ID of the user attending the event.
     */
    public function attendEvent(int $event_id, int $user_id): void
    {
        $validationError = $this->validateEventRegistration($event_id, $user_id);
        if ($validationError) {
            sendResponse(false, $validationError, 400);
        }

        $stmt = $this->pdo->prepare("INSERT INTO `Registration` (event_id, user_id, registered_at) VALUES (?, ?, ?)");
        if ($stmt->execute([$event_id, $user_id, date('Y-m-d H:i:s')])) {
            sendResponse();
        } else {
            sendResponse(false, 'Failed to attend the event.', 500);
        }
    }

    /**
     * Cancels a user's attendance for an event.
     * 
     * @param int $event_id The ID of the event.
     * @param int $user_id The ID of the user cancelling attendance.
     * @return bool True if cancellation was successful, false otherwise.
     */
    public function cancelAttendance($event_id, $user_id)
    {
        $validationError = $this->validateEventCancellation($event_id, $user_id);
        if ($validationError) {
            sendResponse(false, $validationError, 400);
        }

        $stmt = $this->pdo->prepare("DELETE FROM `Registration` WHERE event_id = ? AND user_id = ?");
        if ($stmt->execute([$event_id, $user_id])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if a user can edit an event.
     * 
     * @param int $eventOwnerId The ID of the event owner.
     * @return bool True if the user can edit the event, false otherwise.
     */
    public function canUserEditEvent(int $eventOwnerId)
    {
        $role = $_SESSION['role'] ?? false;
        return $_SESSION['user_id'] === $eventOwnerId || $role === 'Admin';
    }

    /**
     * Checks if a user can delete an event.
     * 
     * @param int $eventOwnerId The ID of the event owner.
     * @param int $userId The ID of the user attempting to delete the event.
     * @return bool True if the user can delete the event, false otherwise.
     */
    private function canUserDeleteEvent(int $eventOwnerId, int $userId)
    {
        $isAdmin = $_SESSION['role'] ?? false;
        return $userId === $eventOwnerId || $isAdmin;
    }

    /**
     * Validates event input data.
     * 
     * @param string $title The title of the event.
     * @param string $datetime The date and time of the event.
     * @param string $location The location of the event.
     * @param string $description The description of the event.
     * @return string An error message if validation fails, empty string otherwise.
     */
    private function validateEventInput(string $title, string $datetime, string $location, string $description)
    {
        if (empty($title) || empty($datetime) || empty($location) || empty($description)) {
            return 'All fields are required.';
        }

        if (strlen($title) > 100) {
            return 'Event title must not exceed 100 characters.';
        }

        if (strlen($location) > 100) {
            return 'Event location must not exceed 100 characters.';
        }

        if (strlen($description) > 255) {
            return 'Event description must not exceed 255 characters.';
        }

        $dateTime = DateTime::createFromFormat('Y-m-d H:i', $datetime);
        if ($dateTime < new DateTime()) {
            return 'Event date must be in the future.';
        }

        return '';
    }

    /**
     * Validates event registration data.
     * 
     * @param int $event_id The ID of the event.
     * @param int $user_id The ID of the user registering for the event.
     * @return string An error message if validation fails, empty string otherwise.
     */
    private function validateEventRegistration(int $event_id, int $user_id): string
    {
        if (empty($event_id) || empty($user_id)) {
            return 'Event ID and User ID are required.';
        }

        $stmt = $this->pdo->prepare('
            SELECT COUNT(*) as count
            FROM Event
            WHERE id = ? AND date > NOW()');

        $stmt->execute([$event_id]);
        $event = $stmt->fetch();
        if ($event === false || $event['count'] === 0) {
            return 'Event not found or already passed.';
        }

        $stmt = $this->pdo->prepare('
            SELECT COUNT(*) as count
            FROM Registration
            WHERE event_id = ? AND user_id = ?');

        $stmt->execute([$event_id, $user_id]);
        $attendance = $stmt->fetch();
        if ($attendance['count'] > 0) {
            return 'You have already attended this event.';
        }

        return '';
    }

    /**
     * Validates event cancellation data.
     * 
     * @param int $event_id The ID of the event.
     * @param int $user_id The ID of the user cancelling attendance.
     * @return string An error message if validation fails, empty string otherwise.
     */
    private function validateEventCancellation(int $event_id, int $user_id): string
    {
        if (empty($event_id) || empty($user_id)) {
            return 'Event ID and User ID are required.';
        }

        $stmt = $this->pdo->prepare('
        SELECT COUNT(*) as count
        FROM Event
        WHERE id = ? AND date > NOW()');

        $stmt->execute([$event_id]);
        $event = $stmt->fetch();
        if ($event === false || $event['count'] === 0) {
            return 'Event not found or already passed.';
        }

        $stmt = $this->pdo->prepare('
        SELECT COUNT(*) as count
        FROM Registration
        WHERE event_id = ? AND user_id = ?');

        $stmt->execute([$event_id, $user_id]);
        $attendance = $stmt->fetch();
        if ($attendance['count'] === 0) {
            return 'You are not registered for this event.';
        }

        return '';
    }

    /**
     * Retrieves the owner ID of an event.
     * 
     * @param int $eventId The ID of the event.
     * @return int|null The ID of the event owner or null if not found.
     */
    private function getEventOwnerId(int $eventId): int|null
    {
        $stmt = $this->pdo->prepare("SELECT created_by FROM `Event` WHERE id = ?");
        $stmt->execute([$eventId]);
        $userId = $stmt->fetchColumn();
        return $userId === false ? null : $userId;
    }
}