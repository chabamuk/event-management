<?php
/**
 * Class UserItem
 *
 * Represents a user item with an ID, username, and role.
 *
 * @package EventManagement\Backend\Contracts
 */
class UserItem
{
    /**
     * @var int $id The unique identifier for the user.
     */
    public int $id;

    /**
     * @var string $username The username of the user.
     */
    public string $username;

    /**
     * @var string $role The role of the user.
     */
    public string $role;

    /**
     * UserItem constructor.
     *
     * @param int $id The unique identifier for the user.
     * @param string $username The username of the user.
     * @param string $role The role of the user.
     */
    public function __construct(int $id, string $username, string $role)
    {
        $this->id = $id;
        $this->username = $username;
        $this->role = $role;
    }
}