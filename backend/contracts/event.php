<?php
/**
 * Class EventItem
 *
 * Represents an event item with details such as id, title, description, date, location, and edit permissions.
 */
class EventItem
{
    /**
     * @var int $id The unique identifier for the event.
     */
    public int $id;

    /**
     * @var string $title The title of the event.
     */
    public string $title;

    /**
     * @var string $description A brief description of the event.
     */
    public string $description;

    /**
     * @var string $date The date of the event.
     */
    public string $date;

    /**
     * @var string $location The location where the event will take place.
     */
    public string $location;

    /**
     * @var bool $canEdit Indicates whether the event can be edited.
     */
    public bool $canEdit;

    /**
     * EventItem constructor.
     *
     * @param int $id The unique identifier for the event.
     * @param string $title The title of the event.
     * @param string $description A brief description of the event.
     * @param string $date The date of the event.
     * @param string $location The location where the event will take place.
     * @param bool $canEdit Indicates whether the event can be edited.
     */
    public function __construct(int $id, string $title, string $description, string $date, string $location, bool $canEdit)
    {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->date = $date;
        $this->location = $location;
        $this->canEdit = $canEdit;
    }
}
