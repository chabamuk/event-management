<?php
require '../contracts/event.php';
/**
 * Class PagedEventsResponse
 *
 * This class represents a paginated response for events.
 *
 * @package EventManagement
 */
class PagedEventsResponse
{
    /**
     * @var array $events List of events.
     */
    public array $events;

    /**
     * @var int $totalPages Total number of pages.
     */
    public int $totalPages;

    /**
     * @var int $currentPage Current page number.
     */
    public int $currentPage;

    /**
     * @var array|null $attendedEvents List of attended events, nullable.
     */
    public ?array $attendedEvents;

    /**
     * @var array|null $canModifyEvents List of events that can be modified, nullable.
     */
    public ?array $canModifyEvents;

    /**
     * PagedEventsResponse constructor.
     *
     * @param array $events List of events.
     * @param int $totalPages Total number of pages.
     * @param int $currentPage Current page number.
     * @param array|null $attendedEvents List of attended events, nullable.
     * @param array|null $canModifyEvents List of events that can be modified, nullable.
     */
    public function __construct(array $events, int $totalPages, int $currentPage, ?array $attendedEvents, ?array $canModifyEvents)
    {
        $this->events = $events;
        $this->totalPages = $totalPages;
        $this->currentPage = $currentPage;
        $this->attendedEvents = $attendedEvents;
        $this->canModifyEvents = $canModifyEvents;
        $this->events = array_map(fn($event) => new EventItem(
            $event['id'],
            $event['title'],
            $event['description'],
            $event['date'],
            $event['location'],
            in_array($event['id'], array_column($events, 'id'))
        ), $events);
    }
}