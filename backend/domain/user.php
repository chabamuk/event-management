<?php
require '../contracts/user.php';
/**
 * Class User
 *
 * Represents a user in the event management system.
 *
 * @package Domain
 */
class User
{
    /**
     * @var int $id The unique identifier for the user.
     */
    private int $id;

    /**
     * @var string $username The username of the user.
     */
    private string $username;

    /**
     * @var string $password The password of the user.
     */
    private string $password;

    /**
     * @var string $role The role of the user.
     */
    private string $role;

    /**
     * User constructor.
     *
     * @param int $id The unique identifier for the user.
     * @param string $username The username of the user.
     * @param string $password The password of the user.
     * @param string $role The role of the user.
     */
    public function __construct(int $id, string $username, string $password, string $role)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->role = $role;
    }

    /**
     * Get the user's unique identifier.
     *
     * @return int The user's unique identifier.
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Get the user's username.
     *
     * @return string The user's username.
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * Get the user's password.
     *
     * @return string The user's password.
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * Get the user's role.
     *
     * @return string The user's role.
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * Find a user by their username.
     *
     * @param PDO $pdo The PDO instance for database connection.
     * @param string $username The username to search for.
     * @return User|null The User object if found, null otherwise.
     */
    public static function findByUsername($pdo, $username): User|null
    {
        $stmt = $pdo->prepare("SELECT * FROM User WHERE username = ?");
        $stmt->execute([$username]);
        $user = $stmt->fetch();
        if ($user) {
            return new User($user['id'], $user['username'], $user['password'], $user['role']);
        }
        return null;
    }
}