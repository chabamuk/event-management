<?php
/**
 * Class Event
 * 
 * Represents an event with details such as title, description, date, location, and creator.
 */
class Event
{
    /**
     * @var int $id The unique identifier for the event.
     */
    private int $id;

    /**
     * @var string $title The title of the event.
     */
    private string $title;

    /**
     * @var string $description A brief description of the event.
     */
    private string $description;

    /**
     * @var string $date The date of the event.
     */
    private string $date;

    /**
     * @var string $location The location where the event will take place.
     */
    private string $location;

    /**
     * @var string $createdBy The creator of the event.
     */
    private string $createdBy;

    /**
     * Event constructor.
     * 
     * @param int $id The unique identifier for the event.
     * @param string $title The title of the event.
     * @param string $description A brief description of the event.
     * @param string $date The date of the event.
     * @param string $location The location where the event will take place.
     * @param string $createdBy The creator of the event.
     */
    public function __construct(int $id, string $title, string $description, string $date, string $location, string $createdBy)
    {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->date = $date;
        $this->location = $location;
        $this->createdBy = $createdBy;
    }

    /**
     * Get the unique identifier for the event.
     * 
     * @return int The unique identifier for the event.
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the unique identifier for the event.
     * 
     * @param int $id The unique identifier for the event.
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * Get the title of the event.
     * 
     * @return string The title of the event.
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Set the title of the event.
     * 
     * @param string $title The title of the event.
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * Get a brief description of the event.
     * 
     * @return string A brief description of the event.
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Set a brief description of the event.
     * 
     * @param string $description A brief description of the event.
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * Get the date of the event.
     * 
     * @return string The date of the event.
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * Set the date of the event.
     * 
     * @param string $date The date of the event.
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * Get the location where the event will take place.
     * 
     * @return string The location where the event will take place.
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * Set the location where the event will take place.
     * 
     * @param string $location The location where the event will take place.
     */
    public function setLocation(string $location): void
    {
        $this->location = $location;
    }

    /**
     * Get the creator of the event.
     * 
     * @return string The creator of the event.
     */
    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    /**
     * Set the creator of the event.
     * 
     * @param string $createdBy The creator of the event.
     */
    public function setCreatedBy(string $createdBy): void
    {
        $this->createdBy = $createdBy;
    }
}
