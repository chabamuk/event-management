var class_event =
[
    [ "__construct", "class_event.html#a11d0be36b1f8fdb8bca53b698fe584ee", null ],
    [ "getCreatedBy", "class_event.html#a55a1348550fd70310e59ba9af3db1830", null ],
    [ "getDate", "class_event.html#a24d89b0ad05ea2e33626b1fc8ed59bc3", null ],
    [ "getDescription", "class_event.html#a2e7bb35c71bf1824456ceb944cb7a845", null ],
    [ "getId", "class_event.html#a12251d0c022e9e21c137a105ff683f13", null ],
    [ "getLocation", "class_event.html#a270a747ff748def87f313beeef64f3b3", null ],
    [ "getTitle", "class_event.html#a95e859a4588a39a1824b717378a84c29", null ],
    [ "setCreatedBy", "class_event.html#a6ca628b352160a71f8b465f764bd89d2", null ],
    [ "setDate", "class_event.html#a53a55501ef12c12efcbc8be7be123ceb", null ],
    [ "setDescription", "class_event.html#a3eda7afea80371b606cd289c66ab3e7c", null ],
    [ "setId", "class_event.html#af8e956b8b0343ff7d1b955c26cb0c780", null ],
    [ "setLocation", "class_event.html#a4c5c6e78f1e8c0ae4bf416d0f0d73ef1", null ],
    [ "setTitle", "class_event.html#a754ef3032cc3ffb25ad2d1a13720fc29", null ]
];