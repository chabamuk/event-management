var common_8php =
[
    [ "checkServerMethod", "common_8php.html#a70c3ea68038b754cb9c128e30bbe85d3", null ],
    [ "checkUserAuthentication", "common_8php.html#a77e6aebd032f5a70b96dd9937a687237", null ],
    [ "continueSession", "common_8php.html#a0d85cb7bf88b76e3270d9e3fc752a8a9", null ],
    [ "getParam", "common_8php.html#a329bfcd74709e3de51c7c3a4970de685", null ],
    [ "getRequestData", "common_8php.html#aee166055c31ccd75b6d4ac78b22b244c", null ],
    [ "isUserLoggedIn", "common_8php.html#aeb08814a3b11252c66e465a868da3ea7", null ],
    [ "sendResponse", "common_8php.html#a1aff41ed1379eb553906029df03fb7f4", null ],
    [ "validateUserInputForXSS", "common_8php.html#a16a0fe9d8873ee81bd43fb398c8cd396", null ]
];