var class_paged_events_response =
[
    [ "__construct", "class_paged_events_response.html#a6ff7d81e75c302219eadf665c61b053f", null ],
    [ "$attendedEvents", "class_paged_events_response.html#a1bc3d8810781219f04cb968a389b0c0c", null ],
    [ "$canModifyEvents", "class_paged_events_response.html#abba5d4ce61a6ab8cc0fa0cafa6149ea0", null ],
    [ "$currentPage", "class_paged_events_response.html#ab0c63208a9a6718cc5ba53d6fd1b7bcb", null ],
    [ "$events", "class_paged_events_response.html#a7f1b586ccc0e88f76dc99a85241f7113", null ],
    [ "$totalPages", "class_paged_events_response.html#a6e7a2e6dd458ad4d561a295efeb4daab", null ]
];