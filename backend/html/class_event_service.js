var class_event_service =
[
    [ "__construct", "class_event_service.html#ae411d4f16044b1093b8bb9a96035f89b", null ],
    [ "attendEvent", "class_event_service.html#a3a8f84a7389f435a2e9393cfe0b07bc1", null ],
    [ "cancelAttendance", "class_event_service.html#aa7df50c8413bb0c616dcfcfb05e74479", null ],
    [ "canUserEditEvent", "class_event_service.html#a027d76b8b2b168d46e6d1330b47a8d78", null ],
    [ "createEvent", "class_event_service.html#a7533503ecbf0c305988d8b32f2381b7b", null ],
    [ "deleteEvent", "class_event_service.html#a2e75e37d61cf5b619e0a3e181bcd337a", null ],
    [ "editEvent", "class_event_service.html#a47a2997c5d918d7d1ec92cb85e1aa0d7", null ],
    [ "getEventById", "class_event_service.html#a3de9a11eefc7e50cac8482a831ca90f1", null ],
    [ "getPagedEvents", "class_event_service.html#a091a96ad0210d1262c0d5d18c95362ce", null ]
];