var annotated_dup =
[
    [ "AuthService", "class_auth_service.html", "class_auth_service" ],
    [ "Event", "class_event.html", "class_event" ],
    [ "EventItem", "class_event_item.html", "class_event_item" ],
    [ "EventService", "class_event_service.html", "class_event_service" ],
    [ "PagedEventsResponse", "class_paged_events_response.html", "class_paged_events_response" ],
    [ "User", "class_user.html", "class_user" ],
    [ "UserItem", "class_user_item.html", "class_user_item" ]
];