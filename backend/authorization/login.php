<?php
require '../db/db.php';
require '../common/common.php';
require '../services/auth-service.php';

/**
 * Checks if the server request method matches the expected method.
 *
 * @param string $method The expected request method (e.g., "POST").
 * @return void
 */
checkServerMethod("POST");

/**
 * Validates user input to prevent XSS attacks.
 *
 * @param string $input The user input to validate.
 * @return string The sanitized user input.
 */
$username = (string) validateUserInputForXSS($_POST['username']);
$password = (string) validateUserInputForXSS($_POST['password']);

/**
 * Sends a response to the client.
 *
 * @param bool $success Indicates whether the operation was successful.
 * @param string $message The message to send in the response.
 * @param int $statusCode The HTTP status code to send in the response.
 * @return void
 */
if (empty($username) || empty($password)) {
    sendResponse(false, "Username and password are required.", 400);
}

/**
 * AuthService class handles authentication-related operations.
 *
 * @param PDO $pdo The PDO instance for database connection.
 */
$authService = new AuthService($pdo);

/**
 * Logs in a user with the provided username and password.
 *
 * @param string $username The username of the user.
 * @param string $password The password of the user.
 * @return void
 */
$authService->login($username, $password);