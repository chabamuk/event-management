<?php
/**
 * Starts a new session or resumes the existing session.
 * 
 * @return void
 */
session_start();

/**
 * Destroys all data registered to a session.
 * 
 * @return void
 */
session_destroy();

/**
 * Outputs a JSON encoded message indicating successful logout.
 * 
 * @return void
 */
echo json_encode(["message" => "Logout successful."]);
