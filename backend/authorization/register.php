<?php
require '../db/db.php';
require '../common/common.php';
require '../services/auth-service.php';

/**
 * Validates that the server request method is the expected method.
 *
 * @param string $method The expected request method (e.g., "POST").
 * @return void
 * @throws Exception If the request method does not match the expected method.
 */
checkServerMethod("POST");

/**
 * Validates user input to prevent XSS attacks.
 *
 * @param string $input The user input to validate.
 * @return string The sanitized user input.
 */
$username = (string) validateUserInputForXSS($_POST['username']);
$password = (string) validateUserInputForXSS($_POST['password']);
$email = (string) validateUserInputForXSS($_POST['email']);

/**
 * Registers a new user with the provided username, password, and email.
 *
 * @param string $username The username of the new user.
 * @param string $password The password of the new user.
 * @param string $email The email address of the new user.
 * @return void
 */
$authService = new AuthService($pdo);
$authService->register($username, $password, $email);
