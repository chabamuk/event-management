<?php
/**
 * Sends a JSON response and terminates the script.
 *
 * @param bool $success Indicates if the response is successful.
 * @param string $error Error message to include in the response.
 * @param int $statusCode HTTP status code for the response.
 * @return never
 */
function sendResponse(bool $success = true, string $error = '', $statusCode = 200): never
{
    http_response_code($statusCode);
    echo json_encode(['success' => $success, 'error' => $error]);
    exit;
}

/**
 * Checks if a user is logged in by verifying the session.
 *
 * @return bool True if the user is logged in, false otherwise.
 */
function isUserLoggedIn(): bool
{
    return isset($_SESSION['user_id']);
}

/**
 * Checks if the user is authenticated. If not, sends an unauthorized response.
 *
 * @return void
 */
function checkUserAuthentication(): void
{
    if (!isUserLoggedIn()) {
        sendResponse(false, 'Unauthorized user', 401);
    }
}

/**
 * Continues the session if a session cookie is present.
 *
 * @return void
 */
function continueSession(): void
{
    if (isset($_COOKIE[session_name()])) {
        session_start();
    }
}

/**
 * Retrieves and decodes JSON data from the request body.
 *
 * @return mixed The decoded JSON data.
 */
function getRequestData(): mixed
{
    return json_decode(file_get_contents('php://input'), true);
}

/**
 * Retrieves a parameter from the GET request and sanitizes it.
 *
 * @param string $param The name of the parameter to retrieve.
 * @return string The sanitized parameter value.
 */
function getParam($param): string
{
    return htmlspecialchars(trim($_GET[$param] ?? ''));
}

/**
 * Checks if the server request method matches the expected method.
 * If not, sends an invalid request method response.
 *
 * @param string $method The expected request method.
 * @return void
 */
function checkServerMethod($method): void
{
    if ($_SERVER['REQUEST_METHOD'] !== $method) {
        sendResponse(false, 'Invalid request method', 405);
    }
}

/**
 * Validates user input to prevent XSS attacks.
 * If invalid input is detected, sends an invalid input response.
 *
 * @param string $input The user input to validate.
 * @return string The validated input.
 */
function validateUserInputForXSS($input): string
{
    $xssPattern = '/(
        <\s*script\b[^>]*>(.*?)<\s*\/\s*script\s*>|
        (javascript|vbscript|data):[^\s]*|
        on\w+\s*=\s*["\'][^"\']*["\']|
        <\s*style\b[^>]*>(.*?)<\s*\/\s*style\s*>|
        style\s*=\s*["\'][^"\']*expression\([^)]*\)["\']|
        <\s*(iframe|object|embed|applet|link|meta|form|img|svg|base|bgsound|audio|video|body)\b[^>]*>
    )/ix';

    if (preg_match($xssPattern, $input)) {
        sendResponse(false, 'Invalid input', 400);
    }

    return $input;
}