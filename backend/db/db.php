<?php
/**
 * Database connection configuration and initialization.
 *
 * This script sets up the connection to the MySQL database using PDO.
 * It defines the database connection parameters and options, and attempts
 * to create a new PDO instance for database interaction.
 *
 * @file db.php
 * @package EventManagement
 * @version 1.0
 */

/**
 * Database host.
 * @var string
 */
$host = 'localhost';

/**
 * Database name.
 * @var string
 */
$db = 'event_management_db';

/**
 * Database user.
 * @var string
 */
$user = 'root';

/**
 * Database charset.
 * @var string
 */
$charset = 'utf8mb4';

/**
 * Data Source Name (DSN) for PDO connection.
 * @var string
 */
$dsn = "mysql:host=$host;dbname=$db;charset=$charset";

/**
 * PDO options for error handling, fetch mode, and prepared statements.
 * @var array
 */
$options = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false,
];

try {
    /**
     * PDO instance for database connection.
     * @var PDO
     */
    $pdo = new PDO($dsn, $user, null, $options);
} catch (\PDOException $e) {
    /**
     * Exception handling for PDO connection errors.
     * Throws a new PDOException with the error message and code.
     * @throws \PDOException
     */
    throw new \PDOException($e->getMessage(), (int) $e->getCode());
}