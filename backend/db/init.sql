CREATE DATABASE IF NOT EXISTS event_management_db;

USE event_management_db;

DROP TABLE IF EXISTS Registration;

DROP TABLE IF EXISTS `Event`;

DROP TABLE IF EXISTS User;

CREATE TABLE
    User (
        id INT AUTO_INCREMENT PRIMARY KEY,
        username VARCHAR(50) NOT NULL UNIQUE,
        password VARCHAR(255) NOT NULL,
        email VARCHAR(100) NOT NULL UNIQUE,
        role ENUM ('User', 'Admin') DEFAULT 'User' NOT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    );

CREATE TABLE
    `Event` (
        id INT AUTO_INCREMENT PRIMARY KEY,
        title VARCHAR(100) NOT NULL,
        location VARCHAR(100) NOT NULL,
        description VARCHAR(255) NOT NULL,
        date DATETIME NOT NULL,
        created_by INT NOT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_by INT NULL,
        updated_at TIMESTAMP NULL,
        FOREIGN KEY (created_by) REFERENCES User (id) ON DELETE CASCADE
    );

CREATE TABLE
    Registration (
        id INT AUTO_INCREMENT PRIMARY KEY,
        user_id INT NOT NULL,
        event_id INT NOT NULL,
        registered_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        FOREIGN KEY (user_id) REFERENCES User (id) ON DELETE CASCADE,
        FOREIGN KEY (event_id) REFERENCES `Event` (id) ON DELETE CASCADE
    );