<?php
require '../db/db.php';
require '../services/event-service.php';

/**
 * Continues the current user session.
 *
 * @return void
 */
continueSession();

/**
 * Checks if the user is authenticated.
 *
 * @return void
 */
checkUserAuthentication();

/**
 * Checks if the server request method matches the expected method.
 *
 * @param string $method The expected request method (e.g., "POST").
 * @return void
 */
checkServerMethod("POST");

/**
 * Retrieves and returns the request data.
 *
 * @return array The request data.
 */
$data = getRequestData();

/**
 * Validates user input to prevent XSS attacks.
 *
 * @param string $input The user input to validate.
 * @return string The validated user input.
 */
$event_id = (int) validateUserInputForXSS($data['eventId']);

/**
 * The user ID from the session.
 *
 * @var int
 */
$user_id = (int) $_SESSION['user_id'];

/**
 * Service class for handling event-related operations.
 *
 * @var EventService
 */
$eventService = new EventService($pdo);

/**
 * Cancels the attendance of a user for a specific event.
 *
 * @param int $event_id The ID of the event.
 * @param int $user_id The ID of the user.
 * @return void
 */
$eventService->cancelAttendance($event_id, $user_id);