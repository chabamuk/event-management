<?php
require '../db/db.php';
require '../services/event-service.php';

/**
 * Continues the current session.
 *
 * @return void
 */
continueSession();

/**
 * Checks if the user is authenticated.
 *
 * @return void
 */
checkUserAuthentication();

/**
 * Checks if the server request method matches the expected method.
 *
 * @param string $method The expected server request method.
 * @return void
 */
checkServerMethod("POST");

/**
 * Validates user input to prevent XSS attacks.
 *
 * @param mixed $input The user input to validate.
 * @return mixed The validated user input.
 */
$eventId = (int) validateUserInputForXSS($_GET['eventId']);
$title = (string) validateUserInputForXSS($_POST['title']);
$datetime = (string) validateUserInputForXSS($_POST['datetime']);
$location = (string) validateUserInputForXSS($_POST['location']);
$description = (string) validateUserInputForXSS($_POST['description']);

/**
 * Retrieves the user ID from the session.
 *
 * @var int $user_id The user ID.
 */
$user_id = (int) $_SESSION['user_id'];

/**
 * EventService constructor.
 *
 * @param PDO $pdo The PDO instance for database connection.
 */
$eventService = new EventService($pdo);

/**
 * Retrieves an event by its ID.
 *
 * @param int $eventId The ID of the event to retrieve.
 * @return Event|null The event object if found, null otherwise.
 */
$event = $eventService->getEventById($eventId);

/**
 * Sends a response to the client.
 *
 * @param bool $success Indicates if the operation was successful.
 * @param string $message The message to send in the response.
 * @param int $statusCode The HTTP status code to send.
 * @return void
 */
if ($event == null) {
    sendResponse(false, 'Event not found', 404);
}

/**
 * Checks if the user is authorized to edit the event.
 *
 * @param int $createdBy The ID of the user who created the event.
 * @return bool True if the user can edit the event, false otherwise.
 */
if (!$eventService->canUserEditEvent($event->getCreatedBy())) {
    sendResponse(false, 'Unauthorized to edit this event.', 403);
}

/**
 * Edits an event with the provided details.
 *
 * @param int $eventId The ID of the event to edit.
 * @param string $title The new title of the event.
 * @param string $datetime The new datetime of the event.
 * @param string $location The new location of the event.
 * @param string $description The new description of the event.
 * @param int $user_id The ID of the user editing the event.
 * @return void
 */
$eventService->editEvent($eventId, $title, $datetime, $location, $description, $user_id);