<?php
require '../db/db.php';
require '../services/event-service.php';

/**
 * Continues the current session.
 *
 * @return void
 */
continueSession();

/**
 * Checks if the user is authenticated.
 *
 * @return void
 */
checkUserAuthentication();

/**
 * Checks if the server request method matches the expected method.
 *
 * @param string $method The expected request method (e.g., "POST").
 * @return void
 */
checkServerMethod("POST");

/**
 * Validates user input to prevent XSS attacks.
 *
 * @param string $input The user input to validate.
 * @return string The sanitized user input.
 */
$title = (string) validateUserInputForXSS($_POST['title']);
$datetime = (string) validateUserInputForXSS($_POST['datetime']);
$location = (string) validateUserInputForXSS($_POST['location']);
$description = (string) validateUserInputForXSS($_POST['description']);
$user_id = (int) $_SESSION['user_id'];

/**
 * Creates a new event.
 *
 * @param string $title The title of the event.
 * @param string $datetime The date and time of the event.
 * @param string $location The location of the event.
 * @param string $description The description of the event.
 * @param int $user_id The ID of the user creating the event.
 * @return void
 */
$eventService = new EventService($pdo);
$eventService->createEvent($title, $datetime, $location, $description, $user_id);