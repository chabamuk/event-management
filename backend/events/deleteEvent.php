<?php
require '../db/db.php';
require '../services/event-service.php';

/**
 * Continues the current session.
 *
 * @return void
 */
continueSession();

/**
 * Checks if the user is authenticated.
 *
 * @return void
 */
checkUserAuthentication();

/**
 * Checks if the server request method matches the expected method.
 *
 * @param string $method The expected server request method.
 * @return void
 */
checkServerMethod("DELETE");

/**
 * Retrieves a parameter from the request.
 *
 * @param string $key The key of the parameter to retrieve.
 * @return mixed|null The value of the parameter or null if not found.
 */
$eventId = getParam('eventId') ?? null;

if (!$eventId) {
    /**
     * Sends a response to the client.
     *
     * @param bool $success Indicates if the operation was successful.
     * @param string $message The message to send in the response.
     * @param int $statusCode The HTTP status code to send.
     * @return void
     */
    sendResponse(false, 'Event ID is required', 400);
}

/**
 * Validates user input to prevent XSS attacks.
 *
 * @param string $input The user input to validate.
 * @return void
 */
validateUserInputForXSS($eventId);

/**
 * EventService constructor.
 *
 * @param PDO $pdo The PDO instance for database interaction.
 */
$eventService = new EventService($pdo);

/**
 * Deletes an event.
 *
 * @param int $eventId The ID of the event to delete.
 * @param int $userId The ID of the user performing the deletion.
 * @return void
 */
$eventService->deleteEvent($eventId, $_SESSION['user_id']);