<?php
require '../db/db.php';
require '../services/event-service.php';

/**
 * Continues the current session if it exists, or starts a new session.
 * This function should be called at the beginning of the script to ensure
 * session variables are available.
 */
continueSession();

/**
 * Checks if the server request method matches the expected method.
 * If the method does not match, it terminates the script with an error.
 *
 * @param string $method The expected server request method (e.g., "GET", "POST").
 */
checkServerMethod("GET");

/**
 * Validates user input to prevent Cross-Site Scripting (XSS) attacks.
 * This function should be used to sanitize any user input that will be
 * output to the browser.
 *
 * @param mixed $input The user input to be validated.
 * @return mixed The sanitized user input.
 */

$page = isset($_GET['page']) ? (int) validateUserInputForXSS($_GET['page']) : 1;
if ($page < 1) {
    $page = 1;
}

/**
 * Validates user input to prevent Cross-Site Scripting (XSS) attacks.
 * This function should be used to sanitize any user input that will be
 * output to the browser.
 *
 * @param mixed $input The user input to be validated.
 * @return mixed The sanitized user input.
 */
$isAttending = isset($_GET['isAttending']) ? (bool) validateUserInputForXSS(filter_var($_GET['isAttending'], FILTER_VALIDATE_BOOLEAN)) : null;
$userId = $_SESSION['user_id'] ?? null;
/**
 * Retrieves a paginated list of events based on the provided parameters.
 *
 * @param int $page The page number to retrieve.
 * @param bool|null $isAttending Optional. Filter events by attendance status.
 * @param int|null $userId Optional. The ID of the user to filter events for.
 * @return array The paginated list of events.
 */
$eventService = new EventService($pdo);
$response = $eventService->getPagedEvents($page, $isAttending, $userId);
echo json_encode($response);