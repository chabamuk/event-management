<?php
require '../db/db.php';
require '../services/event-service.php';

/**
 * Continues the current session.
 *
 * @return void
 */
continueSession();

/**
 * Checks if the server request method matches the expected method.
 *
 * @param string $method The expected request method.
 * @return void
 */
checkServerMethod("GET");

/**
 * Checks if the user is authenticated.
 *
 * @return void
 */
checkUserAuthentication();

/**
 * Retrieves a parameter from the request.
 *
 * @param string $name The name of the parameter.
 * @return mixed|null The value of the parameter or null if not set.
 */
$eventId = getParam('eventId') ?? null;

/**
 * Sends a response to the client.
 *
 * @param bool $success Indicates if the request was successful.
 * @param string $message The response message.
 * @param int $statusCode The HTTP status code.
 * @return void
 */
if (!$eventId) {
    sendResponse(false, 'Event ID is required', 400);
}

/**
 * Validates user input to prevent XSS attacks.
 *
 * @param string $input The user input to validate.
 * @return void
 */
validateUserInputForXSS($eventId);

/**
 * EventService constructor.
 *
 * @param PDO $pdo The PDO instance.
 */
$eventService = new EventService($pdo);

/**
 * Retrieves an event by its ID.
 *
 * @param int $eventId The ID of the event.
 * @return Event|null The event object or null if not found.
 */
$event = $eventService->getEventById($eventId);

/**
 * Sends a response to the client.
 *
 * @param bool $success Indicates if the request was successful.
 * @param string $message The response message.
 * @param int $statusCode The HTTP status code.
 * @return void
 */
if ($event === null) {
    sendResponse(false, 'Event not found', 404);
}

/**
 * EventItem constructor.
 *
 * @param int $id The ID of the event.
 * @param string $title The title of the event.
 * @param string $description The description of the event.
 * @param string $date The date of the event.
 * @param string $location The location of the event.
 * @param bool $canEdit Indicates if the user can edit the event.
 */
$eventDto = new EventItem(
    $event->getId(),
    $event->getTitle(),
    $event->getDescription(),
    $event->getDate(),
    $event->getLocation(),
    $eventService->canUserEditEvent($event->getCreatedBy())
);

/**
 * Encodes data to JSON format.
 *
 * @param mixed $value The data to encode.
 * @return string The JSON encoded string.
 */
echo json_encode($eventDto);