document.addEventListener("DOMContentLoaded", function () {
  const navbar = document.createElement("nav");
  navbar.className = "navbar";

  const menuUl = document.createElement("ul");

  const menuItems = [
    { name: "Events", link: "event-list.php" },
    { name: "Create Event", link: "edit-event.php", auth: isAuthorized },
  ];

  const authItems = [
    { name: "Register", link: "register.php" },
    { name: "Login", link: "login.php" },
  ];
  const profileItems = [{ name: "Logout", link: "login.php", click: logout }];

  // Menu items (event, registrations)
  menuItems.forEach((item) => {
    if (item.auth && item.auth instanceof Function && !item.auth()) {
      return;
    }

    const li = document.createElement("li");
    const a = document.createElement("a");
    a.href = item.link;
    a.textContent = item.name;
    li.appendChild(a);
    menuUl.appendChild(li);
  });

  navbar.appendChild(menuUl);

  // Profile items (profile, logout) or auth items (register, login) depending on the user's authentication status
  const profileUl = document.createElement("ul");

  (isAuthorized() ? profileItems : authItems).forEach((item) => {
    const li = document.createElement("li");
    const a = document.createElement("a");
    if (item.link) {
      a.href = item.link;
    }
    if (item.click && item.click instanceof Function) {
      a.addEventListener("click", item.click);
    }
    a.textContent = item.name;
    li.appendChild(a);
    profileUl.appendChild(li);
  });

  navbar.appendChild(profileUl);
  document.body.insertBefore(navbar, document.body.firstChild);
});
