document.addEventListener("DOMContentLoaded", async function () {
  if (!isAuthorized()) {
    redirectTo("login.php");
    return;
  }

  const form = document.getElementById("createEventForm");

  const urlParams = new URLSearchParams(window.location.search);
  if (urlParams.has("eventId")) {
    const eventId = urlParams.get("eventId");
    try {
      const response = await fetch(
        `../backend/events/getEvent.php?eventId=${eventId}`
      );
      const data = await response.json();
      if (data) {
        populateForm(data, data.canEdit);
      } else {
        handleResult(
          "message",
          "An error occurred while loading the event.",
          false
        );
      }
    } catch (error) {
      handleResult(
        "message",
        "An error occurred while loading the event.",
        false
      );
    }
  }

  form.addEventListener("submit", async function (event) {
    event.preventDefault();

    if (!validateUserInput()) {
      return handleResult("message", "Invalid input", false);
    }

    const title = form.elements["title"].value;
    let datetime = form.elements["datetime"].value;
    const location = form.elements["location"].value;
    const description = form.elements["description"].value;

    const validationError = validateInput(
      title,
      datetime,
      location,
      description
    );
    if (validationError) {
      handleResult(
        "message",
        validationError || "An error occurred while creating the event.",
        false
      );
      return;
    }

    datetime = formatDateTime(datetime);
    const formData = new FormData(form);
    formData.set("datetime", datetime);

    try {
      const url = urlParams.has("eventId")
        ? `../backend/events/editEvent.php?eventId=${urlParams.get("eventId")}`
        : "../backend/events/createEvent.php";

      const response = await fetch(url, { method: "POST", body: formData });
      const data = await response.json();

      if (data.success) {
        handleResult("message", "Event created successfully!");
        form.reset();
        redirectTo("event-list.php");
      } else {
        handleResult(
          "message",
          data.error || "An error occurred while creating the event.",
          false
        );
      }
    } catch (error) {
      handleApiError("An unexpected error occurred.");
    }
  });

  function populateForm(event, canEdit) {
    form.elements["title"].value = event.title;
    form.elements["datetime"].value = event.date;
    form.elements["location"].value = event.location;
    form.elements["description"].value = event.description;

    if (!canEdit) {
      form.elements["title"].disabled = true;
      form.elements["datetime"].disabled = true;
      form.elements["location"].disabled = true;
      form.elements["description"].disabled = true;
      const submitButton = document.getElementById("submitButton");
      form.removeChild(submitButton);
    } else {
      const submitButton = document.getElementById("submitButton");
      submitButton.textContent = "Save Event";
    }
  }

  function validateInput(title, datetime, location, description) {
    if (!title || !datetime || !location || !description) {
      return "All fields are required.";
    }

    if (title.length > 100) {
      return "Event title must not exceed 100 characters.";
    }

    if (location.length > 100) {
      return "Event location must not exceed 100 characters.";
    }

    if (description.length > 255) {
      return "Event description must not exceed 255 characters.";
    }

    const dateTime = new Date(datetime);
    if (dateTime < new Date()) {
      return "Event date must be in the future.";
    }

    return "";
  }

  function formatDateTime(datetime) {
    const date = new Date(datetime);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, "0");
    const day = String(date.getDate()).padStart(2, "0");
    const hours = String(date.getHours()).padStart(2, "0");
    const minutes = String(date.getMinutes()).padStart(2, "0");
    return `${year}-${month}-${day} ${hours}:${minutes}`;
  }
});
