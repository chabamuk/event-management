// Utility function to sanitize input for XSS prevention
function validateUserInputForXSS(input) {
  const xssPattern = new RegExp(
    `<\\s*script\\b[^>]*>(.*?)<\\s*\\/\\s*script\\s*>|` +
      `(javascript|vbscript|data):[^\\s]*|` +
      `on\\w+\\s*=\\s*["'][^"']*["']|` +
      `<\\s*style\\b[^>]*>(.*?)<\\s*\\/\\s*style\\s*>|` +
      `style\\s*=\\s*["'][^"']*expression\\([^)]*\\)["']|` +
      `<\\s*(iframe|object|embed|applet|link|meta|form|img|svg|base|bgsound|audio|video|body)\\b[^>]*>`,
    "i"
  );

  if (xssPattern.test(input)) {
    throw new Error("Invalid input");
  }

  return input;
}

// Usage example for sanitizing input before rendering
function validateUserInput() {
  // Assume this is an input field on your page
  const userInputFields = [
    ...document.querySelectorAll("input"),
    ...document.querySelectorAll("textarea"),
  ];

  const errors = [];
  userInputFields.forEach((field) => {
    // Get the raw user input
    const userInput = field.value;
    try {
      // Validate the input for XSS
      const sanitizedInput = validateUserInputForXSS(userInput);
      // Display the sanitized input on the page
      field.value = sanitizedInput;
    } catch (error) {
      console.error(error.message);
      errors.push(error.message);
    }
  });

  return errors.length === 0;
}

function handleResult(
  messageContainerId,
  message = "success-message",
  isSuccess = true
) {
  const container = document.getElementById(messageContainerId);

  container.className = isSuccess ? "success-message" : "error-message";
  container.textContent = message;
}

function handleApiError(message) {
  const container = document.getElementById(messageContainerId);

  const existingMessages = document.querySelectorAll(".error-message");
  existingMessages.forEach((msg) => msg.remove());

  container.className = "error-message";
  container.textContent = message;
}

function redirectTo(url) {
  window.location.href = url;
}
