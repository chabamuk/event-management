// Load events for a specific page

let eventFilter = {};

const baseBackendPath = "../backend/events";
async function loadEvents(page = 1, filter = {}) {
  try {
    const response = await fetch(
      `${baseBackendPath}/getPagedEvents.php?page=${page}&${new URLSearchParams(
        filter
      )}`
    );
    const data = await response.json();
    updateEventTable(data.events, data.attendedEvents, data.canModifyEvents);
    updatePagination(data.totalPages, data.currentPage);
  } catch (error) {
    console.error("Error fetching events:", error);
  }
}

// Update the event table with new data
function updateEventTable(events, attendedEvents, canModifyEvents) {
  const tbody = document.querySelector("#eventTable tbody");
  tbody.innerHTML = "";

  if (events.length === 0) {
    const row = document.createElement("tr");
    const cell = document.createElement("td");
    cell.colSpan = attendedEvents !== null ? 5 : 4;
    cell.textContent = "No events found";
    cell.classList.add("no-events");
    row.appendChild(cell);
    tbody.appendChild(row);
    return;
  }

  const header = document.getElementById("eventTableHeader");
  if (isAuthorized()) {
    header.innerHTML = `
      <tr>
        <th>Title</th>
        <th>Date</th>
        <th>Location</th>
        <th>Description</th>
        <th>Actions</th>
      </tr>
    `;
  } else {
    header.innerHTML = `
      <tr>
        <th>Title</th>
        <th>Date</th>
        <th>Location</th>
        <th>Description</th>
      </tr>
    `;
  }

  events.forEach((event) => {
    const row = document.createElement("tr");
    const isUserAuthorized = isAuthorized();
    row.innerHTML = `
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      ${
        isUserAuthorized
          ? getColumnAction(event.id, attendedEvents, canModifyEvents)
          : ""
      }
    `;
    const tds = row.getElementsByTagName("td");
    tds[0].innerText = event.title;
    tds[1].innerText = `${new Date(event.date).toLocaleDateString()} ${new Date(
      event.date
    ).toLocaleTimeString()}`;
    tds[2].innerText = event.location;
    tds[3].innerText = event.description;
    if (isUserAuthorized) {
      const attendButton = row.getElementsByClassName("attend-button")[0];
      const cancelAttendanceButton = row.getElementsByClassName(
        "cancel-attendance-button"
      )[0];
      const deleteButton = row.getElementsByClassName("delete-button")[0];
      const detailButton = row.getElementsByClassName("details-button")[0];
      const editButton = row.getElementsByClassName("edit-button")[0];
      if (attendButton) {
        attendButton.addEventListener("click", () => attendEvent(event.id));
      }
      if (cancelAttendanceButton) {
        cancelAttendanceButton.addEventListener("click", () =>
          cancelAttendance(event.id)
        );
      }
      if (deleteButton) {
        deleteButton.addEventListener("click", async () => {
          deleteEvent(event.id);
        });
      }
      if (detailButton) {
        detailButton.addEventListener("click", () => {
          redirectTo(`edit-event.php?eventId=${event.id}`);
        });
      }
      if (editButton) {
        editButton.addEventListener("click", () => {
          redirectTo(`edit-event.php?eventId=${event.id}`);
        });
      }
    }

    tbody.appendChild(row);
  });
}

// Create a pagination button
function createPaginationButton(text, isEnabled, onClick) {
  const button = document.createElement("button");
  button.textContent = text;
  button.classList.add("page-link");
  if (isEnabled) {
    button.addEventListener("click", onClick);
  } else {
    button.disabled = true;
  }
  return button;
}

// Update the pagination controls
function updatePagination(totalPages, currentPage) {
  const pagination = document.getElementById("eventTablePagination");
  pagination.innerHTML = "";

  // First arrow
  pagination.appendChild(
    createPaginationButton("<<", currentPage > 1, () =>
      loadEvents(1, eventFilter)
    )
  );

  // Previous arrow
  pagination.appendChild(
    createPaginationButton("<", currentPage > 1, () =>
      loadEvents(currentPage - 1, eventFilter)
    )
  );

  // Page numbers
  const pageNumberContainer = document.createElement("div");
  pageNumberContainer.classList.add("page-number-container");
  for (let i = 1; i <= totalPages; i++) {
    if (i >= currentPage - 1 && i <= currentPage + 1) {
      const pageLink = createPaginationButton(i, i !== currentPage, () =>
        loadEvents(i, eventFilter)
      );
      if (i === currentPage) {
        pageLink.classList.add("active");
      }
      pageNumberContainer.appendChild(pageLink);
    } else if (i === currentPage - 2 || i === currentPage + 2) {
      const dots = document.createElement("span");
      dots.textContent = "...";
      dots.classList.add("page-dots");
      pageNumberContainer.appendChild(dots);
    }
  }
  pagination.appendChild(pageNumberContainer);

  // Next arrow
  pagination.appendChild(
    createPaginationButton(">", currentPage < totalPages, () =>
      loadEvents(currentPage + 1, eventFilter)
    )
  );

  // Last arrow
  pagination.appendChild(
    createPaginationButton(">>", currentPage < totalPages, () =>
      loadEvents(totalPages, eventFilter)
    )
  );
}

function getColumnAction(eventId, attendedEventsIds, canModifyEvents) {
  const isAttending = attendedEventsIds?.includes(eventId);
  const className = isAttending
    ? "cancel-attendance-button button-error"
    : "attend-button button-primary";
  const buttonText = isAttending ? "Cancel attendance" : "Attend";
  const attendButton = getActionButton(className, buttonText);

  const canModify = canModifyEvents?.includes(eventId);
  const deleteButton = canModify
    ? getActionButton("delete-button button-error", "Delete")
    : "";
  const detailButton = getActionButton(
    "details-button button-secondary",
    "Details"
  );
  const editButton = getActionButton("edit-button button-warning", "Edit");
  return `<td><div class="column-action">${attendButton}${
    canModify ? editButton : detailButton
  }${deleteButton}</div></td>`;
}

function getActionButton(className, buttonText) {
  return `<button class="${className}">${buttonText}</button>`;
}

async function attendEvent(eventId) {
  try {
    await fetch(`${baseBackendPath}/attendEvent.php`, {
      method: "POST",
      body: JSON.stringify({ eventId }),
    });
    loadEvents(1, eventFilter);
  } catch (error) {
    console.error("Error attending event:", error);
  }
}

async function cancelAttendance(eventId) {
  try {
    await fetch(`${baseBackendPath}/cancelAttendance.php`, {
      method: "POST",
      body: JSON.stringify({ eventId }),
    });
    loadEvents(1, eventFilter);
  } catch (error) {
    console.error("Error canceling attendance:", error);
  }
}

async function deleteEvent(eventId) {
  if (confirm("Are you sure you want to delete this event?")) {
    try {
      await fetch(`${baseBackendPath}/deleteEvent.php?eventId=${eventId}`, {
        method: "DELETE",
      });
      loadEvents(1, eventFilter);
    } catch (error) {
      console.error("Error deleting event:", error);
    }
  }
}

// Initial load
loadEvents();

document
  .getElementById("eventListFilterForm")
  .addEventListener("change", function () {
    const form = document.getElementById("eventListFilterForm");
    const formData = new FormData(form);
    formData.set("isAttending", form.elements["isAttending"].checked);
    for (const [key, value] of formData) {
      eventFilter[key] = value;
    }
  });

document.addEventListener("DOMContentLoaded", function () {
  if (!isAuthorized()) {
    const filterElement = document.getElementById("eventListFilter");
    if (filterElement) {
      filterElement.remove();
    }
    return;
  }

  const form = document.getElementById("eventListFilterForm");

  form.addEventListener("submit", function (event) {
    event.preventDefault();
    validateUserInput();
    loadEvents(1, eventFilter);
  });
});
