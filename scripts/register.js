document.addEventListener("DOMContentLoaded", function () {
  if (isAuthorized()) {
    redirectTo("event-list.php");
  }
});

document
  .getElementById("registerForm")
  .addEventListener("submit", handleFormSubmit);

document.getElementById("password").addEventListener("input", validatePassword);

document
  .getElementById("confirmPassword")
  .addEventListener("input", validatePasswordMatch);

async function handleFormSubmit(e) {
  e.preventDefault();
  if (!validateUserInput()) {
    return handleResult("message", "Invalid input", false);
  }

  const formData = getFormData();
  const errorMessage = validateFormData(formData);

  if (errorMessage) {
    return handleResult("message", errorMessage, false);
  }

  try {
    const response = await submitForm(formData);
    const data = await response.json();

    if (data.error) {
      return handleResult("message", data.error, false);
    }

    if (response.ok) {
      redirectTo("login.php");
    }
  } catch (error) {
    return handleResult("message", "An unexpected error occurred.", false);
  }
}

function validatePassword() {
  const password = this.value;
  const requirements = [
    { id: "lengthRequirement", isValid: password.length >= 8 },
    { id: "upperRequirement", isValid: /[A-Z]/.test(password) },
    { id: "lowerRequirement", isValid: /[a-z]/.test(password) },
    { id: "numberRequirement", isValid: /\d/.test(password) },
  ];

  requirements.forEach(({ id, isValid }) => {
    document.getElementById(id).classList.toggle("valid-requirement", isValid);
  });

  validatePasswordMatch();
}

function validatePasswordMatch() {
  const password = document.getElementById("password").value;
  const confirmPassword = document.getElementById("confirmPassword").value;
  const matchRequirement = document.getElementById("matchRequirement");

  matchRequirement.classList.toggle(
    "valid-requirement",
    password === confirmPassword
  );
}

function getFormData() {
  return {
    username: document.getElementById("username").value,
    email: document.getElementById("email").value,
    password: document.getElementById("password").value,
    confirmPassword: document.getElementById("confirmPassword").value,
  };
}

function validateFormData({ password, confirmPassword }) {
  if (password.length < 8) {
    return "Password must be at least 8 characters long.";
  }

  const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/;
  if (!passwordRegex.test(password)) {
    return "Password must contain at least one uppercase letter, one lowercase letter, and one number.";
  }

  if (password !== confirmPassword) {
    return "Passwords do not match!";
  }

  return null;
}

async function submitForm({ username, email, password }) {
  const baseBackendPath = "../backend/authorization";
  return fetch(`${baseBackendPath}/register.php`, {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    body: new URLSearchParams({ username, password, email }),
  });
}
