document.addEventListener("DOMContentLoaded", function () {
  if (isAuthorized()) {
    redirectTo("event-list.php");
  }
});

document.getElementById("loginForm").addEventListener("submit", async (e) => {
  e.preventDefault();
  if (!validateUserInput()) {
    return handleResult("message", "Invalid input", false);
  }

  const username = document.getElementById("loginUsername").value;
  const password = document.getElementById("loginPassword").value;

  var baseBackendPath = "../backend/authorization";
  const response = await fetch(`${baseBackendPath}/login.php`, {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    body: new URLSearchParams({ username, password }),
  });

  const data = await response.json();
  if (!response.ok) {
    return handleResult("message", data.error, false);
  }
  handleResult("message", "Login successful! Redirecting...");
  document.getElementById("loginForm").reset();
  redirectTo("event-list.php");
});
