<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="index.css" />
  <title>Event Management App</title>
</head>

<body>
  <div class="loading-page">
    <svg viewBox="25 25 50 50">
      <circle r="20" cy="50" cx="50"></circle>
    </svg>
  </div>

  <script src="./scripts/common.js"></script>
  <script src="./scripts/loading-page.js"></script>
</body>

</html>