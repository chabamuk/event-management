<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="../styles/index.css">
</head>

<body>
    <div id="navbar-container"></div>
    <div id="registerPageContainer" class="page">
        <div id="registerContainer">
            <h2>Register</h2>
            <div id="message"></div>
            <form id="registerForm">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" id="username" placeholder="Username" required>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" id="email" placeholder="Email" required>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" id="password" placeholder="Password" required>
                </div>
                <ul class="password-requirements" id="passwordRequirements">
                    <li id="lengthRequirement" class="invalid">At least 8 characters</li>
                    <li id="upperRequirement" class="invalid">At least one uppercase letter</li>
                    <li id="lowerRequirement" class="invalid">At least one lowercase letter</li>
                    <li id="numberRequirement" class="invalid">At least one number</li>
                </ul>
                <div class="form-group">
                    <label for="confirmPassword">Confirm Password</label>
                    <input type="password" id="confirmPassword" placeholder="Confirm Password" required>
                </div>
                <ul class="password-requirements" id="confirmPasswordRequirements">
                    <li id="matchRequirement" class="invalid">Password have to match</li>
                </ul>
                <button class="button-primary" type="submit">Register</button>
            </form>
            <small>Already have an account? <a class="link" href="login.php">Login</a></small>
        </div>
    </div>
    <script src="../scripts/user-service.js"></script>
    <script src="../scripts/register.js"></script>
    <script src="../scripts/common.js"></script>
    <script src="../scripts/navbar.js"></script>
</body>

</html>