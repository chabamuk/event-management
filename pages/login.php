<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="../styles/index.css">
</head>

<body>
    <div id="navbar-container"></div>
    <div id="loginPageContainer" class="page">
        <div id="loginContainer">
            <h2>Login</h2>
            <div id="message"></div>
            <form id="loginForm">
                <div class="form-group">
                    <label for="loginUsername">Username</label>
                    <input type="text" id="loginUsername" placeholder="Username" required>
                </div>
                <div class="form-group">
                    <label for="loginPassword">Password</label>
                    <input type="password" id="loginPassword" placeholder="Password" required>
                </div>
                <button class="button-primary" type="submit">Login</button>
            </form>
            <small>Don't have an account? <a class="link" href="register.php">Register</a></small>
        </div>
    </div>
    <script src="../scripts/user-service.js"></script>
    <script src="../scripts/login.js"></script>
    <script src="../scripts/common.js"></script>
    <script src="../scripts/navbar.js"></script>
</body>

</html>