<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create Event</title>
    <link rel="stylesheet" href="../styles/index.css">
    <script src="../scripts/user-service.js" async defer></script>
</head>

<body>
    <div id="navbar-container"></div>
    <div id="createEventPageContainer" class="page">
        <div class="create-event-container">
            <h2>Create Event</h2>
            <form id="createEventForm">
                <div id="message"></div>
                <div class="form-group">
                    <label for="title">Event Title</label>
                    <input type="text" id="title" name="title" required>
                </div>
                <div class="form-group">
                    <label for="datetime">Date & Time</label>
                    <input type="datetime-local" id="datetime" name="datetime" required>
                </div>
                <div class="form-group">
                    <label for="location">Location</label>
                    <input type="text" id="location" name="location" required>
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea id="description" name="description" rows="4" required></textarea>
                </div>
                <button id="submitButton" class="button-primary" type="submit">Create Event</button>
            </form>
        </div>
    </div>


    <script src="../scripts/edit-event.js"></script>
    <script src="../scripts/common.js"></script>
    <script src="../scripts/navbar.js"></script>
</body>

</html>