<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Event List</title>
    <link rel="stylesheet" href="../styles/index.css">
</head>

<body>
    <div id="navbar-container"></div>
    <div id="eventListPageContainer" class="page">
        <div id="eventListFilter">
            <div id="message"></div>
            <form id="eventListFilterForm">
                <div class="form-group">
                    <label for="isAttending">Is Attending</label>
                    <input type="checkbox" id="isAttending" name="isAttending">
                </div>
                <button class="button-primary" type="submit">Filter</button>
            </form>
        </div>
        <div class="event-container">
            <h2>Event List</h2>
            <div id="eventTableContainer">
                <table id="eventTable">
                    <thead id="eventTableHeader">
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div id="eventTablePagination">
            </div>
        </div>
    </div>
    <script src="../scripts/user-service.js"></script>
    <script src="../scripts/event-list.js"></script>
    <script src="../scripts/common.js"></script>
    <script src="../scripts/navbar.js"></script>
</body>

</html>